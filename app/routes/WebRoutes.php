<?php

namespace App\routes;

class WebRoutes
{
    public $router;

    public function __construct()
    {
        $this->router = require_once "routes.php";
    }
}
