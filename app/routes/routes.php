<?php

use App\controllers\MigrationsController;
use App\controllers\SeederController;
use App\controllers\Students\StudentStatisticsController;
use Phroute\Phroute\RouteCollector;

$routerCollector = new RouteCollector();

$routerCollector->get('/run-migrations', [MigrationsController::class, "run"]);
$routerCollector->get('/run-seeders', [SeederController::class, "initializeDatabase"]);

$routerCollector->get('/students/{id:\d+}', [StudentStatisticsController::class, "getStudent"]);

return $routerCollector;
