<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Board extends Model
{
    const CSM_TYPE = "CSM";
    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Has many students.
     *
     * @return HasMany
     */
    public function students(): HasMany
    {
        return $this->hasMany(Student::class, 'board_id', 'id');
    }

    /**
     * Board always belongs to many schools.
     *
     * @return BelongsToMany
     */
    public function schools(): BelongsToMany
    {
        return $this->belongsToMany(School::class, 'board_school', 'board_id', 'school_id');
    }

    /**
     * It checks board type.
     *
     * @return bool
     */
    public function isCSM(): bool
    {
        return $this->type === self::CSM_TYPE;
    }
}
