<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Migration extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
