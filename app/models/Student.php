<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Student extends Model
{
    const MINIMUM_GRADES = 2;
    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Has only one board.
     *
     * @return HasOne
     */
    public function board(): HasOne
    {
        return $this->hasOne(Board::class, 'id', 'board_id');
    }

    /**
     * Has many grades.
     *
     * @return HasMany
     */
    public function grades(): HasMany
    {
       return $this->hasMany(Grade::class, 'student_id', 'id');
    }

    /**
     * Check does student pass board.
     *
     * @return bool
     */
    public function hasPassed(): bool
    {
        if ($this->board->isCSM()) {
            $avgGrade = $this->grades->average('grade_value');

            return  $avgGrade >= $this->board->min_avg_grade;
        }

        return $this->hasEnoughGradesForPassing() && $this->hasPassingGrade();
    }

    /**
     * Check does student have enough grades for passing board.
     *
     * @return bool
     */
    private function hasEnoughGradesForPassing(): bool
    {
        return $this->grades()->count() >= self::MINIMUM_GRADES;
    }

    /**
     * Check does student have best grade greater than minimum grade for passing.
     *
     * @return bool
     */
    private function hasPassingGrade(): bool
    {
        return $this->grades->max('grade_value') >= $this->board->min_avg_grade;
    }
}
