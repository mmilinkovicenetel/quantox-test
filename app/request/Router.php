<?php

namespace App\request;

use App\routes\WebRoutes;
use Phroute\Phroute\Dispatcher;

class Router
{
    private static $router = null;
    private $webRoutes;

    private function __construct()
    {
        $this->webRoutes = new WebRoutes();
    }

    /**
     * Return singleton of Router object.
     *
     * @return Router
     */
    public static function getInstance(): self
    {
        if (is_null(self::$router)) {
            self::$router = new Router();
        }

        return self::$router;
    }

    /**
     * Handle request and return proper response.
     */
    public function handle()
    {
        $response = (new Dispatcher($this->webRoutes->router->getData()))->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
        echo $response;
    }

}
