<?php

namespace App\interfaces\service;

interface ServiceDefaultInterface
{
    public function execute();
}
