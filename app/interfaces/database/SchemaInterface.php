<?php


namespace App\interfaces\database;


interface SchemaInterface
{
    public function execute();
}
