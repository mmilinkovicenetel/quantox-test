<?php

namespace App\controllers\Students;

use App\controllers\Resources\StudentStatisticResource;
use App\models\Student;
use App\Services\Students\StudentsAccomplishmentService;

class StudentStatisticsController
{
    private $studentAccomplishmentService;

    public function __construct()
    {
        $this->studentAccomplishmentService = new StudentsAccomplishmentService();
    }

    /**
     * Get student statistics.
     *
     * @param int $id
     * @return mixed
     */
    public function getStudent(int $id)
    {
        $student = Student::find($id);

        $studentStatistics = $this->studentAccomplishmentService->setStudent($student)->execute();

        return (new StudentStatisticResource())->prepareForResponse($studentStatistics);
    }
}
