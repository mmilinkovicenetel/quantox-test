<?php

namespace App\controllers;

use App\database\Handler;

class MigrationsController
{
    private $migrationHandler;
    private $schemas;
    private $executedSchemas = [];

    public function __construct()
    {
        $this->migrationHandler = new Handler();
        $this->schemas = $this->migrationHandler->getSchemasReadyForMigration();
    }

    /**
     * Run migrations.
     *
     * @return string
     */
    public function run(): string
    {
        foreach ($this->schemas as $schemaClass) {
            $schemaTable = new $schemaClass();
            $schemaTable->execute();
            $this->executedSchemas[] = $schemaClass;
        }
        $this->migrationHandler->saveSchemas($this->executedSchemas);

        return json_encode(["message" => "Successful migration."]);
    }

}
