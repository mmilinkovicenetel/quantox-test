<?php

namespace App\controllers\Resources;

use App\models\Student;

class StudentStatisticResource
{
    private $response;

    public function prepareForResponse(array $studentStatistics)
    {
        $student = Student::find($studentStatistics['student_id']);

        if ($student->board->isCSM()) {
            $this->convertToJson($studentStatistics);
        } else {
            $this->convertToXML($studentStatistics);
        }

        return $this->response;
    }

    /**
     * Convert array to json.
     *
     * @param $studentStatistics
     * @return void
     */
    private function convertToJson($studentStatistics): void
    {
        $this->response = json_encode($studentStatistics);
    }

    /**
     * Convert to XML.
     *
     * @param $studentStatistics
     * @return mixed
     */
    private function convertToXML($studentStatistics)
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
        $this->convertArrayToXml($studentStatistics, $xml);

        $domxml = new \DOMDocument('1.0');
        $domxml->preserveWhiteSpace = false;
        $domxml->formatOutput = true;
        
        $this->response = $xml->asXML();
    }

    /**
     * Parse array to xml response.
     *
     * @param $array
     * @param $xml
     */
    private function convertArrayToXml($array, &$xml) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml->addChild($key);
                    $this->convertArrayToXml($value, $subnode);
                } else {
                    $this->convertArrayToXml($value, $subnode);
                }
            } else {
                $xml->addChild($key, $value);
            }
        }
    }
}
