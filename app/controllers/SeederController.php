<?php


namespace App\controllers;


use App\database\seeds\InitSeeder;

class SeederController
{
    private $databaseInitialization;

    public function __construct()
    {
        $this->databaseInitialization = new InitSeeder();
    }

    public function initializeDatabase()
    {
        $this->databaseInitialization->run();
        
        return json_encode(["message" => "Successful seeding."]);
    }
}
