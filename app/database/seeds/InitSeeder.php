<?php

namespace App\database\seeds;

use App\models\Board;
use App\models\Grade;
use App\models\School;
use App\models\Student;

class InitSeeder
{
    private $boards = [
        ["name" => "Novice", "type" => "CSM"],
        ["name" => "Advanced", "type" => "CSM"],
        ["name" => "Professional", "type" => "CSMB"],
        ["name" => "Professional-Advance", "type" => "CSMB"],
    ];
    private $schools = [
        ["name" => "St. John High School"],
        ["name" => "St. Patrick High School"],
        ["name" => "UCLA High School"],
    ];
    private $students = [
        ["name" => "Pera Peric"],
        ["name" => "Mika Mikic"],
        ["name" => "Zika Zikic"],
        ["name" => "Raja Rajic"],
    ];
    private $grades = [
        ["grade_value" => "5", "subject" => "Math"],
        ["grade_value" => "6", "subject" => "Math"],
        ["grade_value" => "7", "subject" => "Math"],
        ["grade_value" => "5", "subject" => "Science"],
        ["grade_value" => "9", "subject" => "Science"],
        ["grade_value" => "10", "subject" => "Science"],
    ];

    public function __construct()
    {
    }

    public function run()
    {
        $this->fillWithBoards();
        $this->fillWithSchools();
        $this->fillWithStudents();
        $this->fillWithGrades();
        $this->connectBoardsAndSchools();
    }

    private function fillWithBoards()
    {
        foreach ($this->boards as $board) {
            (new Board($board))->save();
        }
    }

    private function fillWithSchools()
    {
        foreach ($this->schools as $school) {
            (new School($school))->save();
        }
    }

    private function fillWithStudents()
    {
        $boards = Board::all();
        foreach ($this->students as $student) {
            $randomBoard = $boards[rand(0, count($boards) - 1)];

            $studentObj = new Student($student);
            $studentObj->board_id = $randomBoard->id;
            $studentObj->save();
        }
    }

    private function fillWithGrades()
    {
        $students = Student::all();

        foreach ($this->grades as $grade) {
            $randomStudent = $students[rand(0, count($students) - 1)];

            $gradeObj = new Grade($grade);
            $gradeObj->student_id = $randomStudent->id;
            $gradeObj->save();
        }
    }

    private function connectBoardsAndSchools()
    {
        $schools = School::all();

        foreach ($schools as $school) {
            $school->boards()->attach(Board::all()->pluck("id"));
        }
    }
}
