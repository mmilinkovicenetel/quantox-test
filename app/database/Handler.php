<?php

namespace App\database;

use App\models\Migration;

class Handler
{
    private $schemasReady = [];
    private $schemasFinished = [];

    public function __construct()
    {
        $this->schemasFinished = $this->getAllSchemaNamesFromMigration();
        $this->schemasReady = require_once (__DIR__ . "/migration_tracking/ReadySchemas.php");
    }

    /**
     * Return array of migrations ready for execute.
     *
     * @return array
     */
    public function getSchemasReadyForMigration(): array
    {
        return array_diff($this->schemasReady, $this->schemasFinished);
    }

    /**
     * Save executed schemas to database.
     *
     * @param array $executedSchemas
     * @return void
     */
    public function saveSchemas(array $executedSchemas): void
    {
        foreach ($executedSchemas as $executedSchema) {
            (new Migration(["schema_name" => $executedSchema]))->save();
        }
    }

    /**
     * Get all schemas from database that have been executed.
     *
     * @return array
     */
    private function getAllSchemaNamesFromMigration(): array
    {
        return array_column(Migration::all()->toArray(), "schema_name");
    }

}
