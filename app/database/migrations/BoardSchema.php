<?php

namespace App\database\migrations;

class BoardSchema extends DatabaseSchema
{
    public function execute()
    {
        $this->databaseSchemaInstance->create("boards", function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', ['CSM', 'CSMB']);
            $table->timestamps();
        });
    }
}
