<?php

namespace App\database\migrations;

class StudentsSchema extends DatabaseSchema
{
    public function execute()
    {
        $this->databaseSchemaInstance->create('students', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('board_id')->unsigned();
            $table->timestamps();

            $table->foreign('board_id')->references('id')->on('boards');
        });
    }
}
