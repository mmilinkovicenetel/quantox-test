<?php

namespace App\database\migrations;

use App\interfaces\database\SchemaInterface;
use Illuminate\Database\Capsule\Manager AS Capsule;

abstract class DatabaseSchema implements SchemaInterface
{
    protected $databaseSchemaInstance;

    public function __construct()
    {
        $this->databaseSchemaInstance = Capsule::schema();
    }

    public abstract function execute();
}
