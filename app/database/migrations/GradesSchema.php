<?php

namespace App\database\migrations;

class GradesSchema extends DatabaseSchema
{

    public function execute()
    {
        $this->databaseSchemaInstance->create('grades', function ($table){
            $table->increments('id');
            $table->enum('grade_value', [5,6,7,8,9,10]);
            $table->string('subject');
            $table->integer('student_id')->unsigned();
            $table->timestamps();

            $table->foreign('student_id')->references('id')->on('students');
        });
    }
}
