<?php

namespace App\database\migrations;

class SchoolSchema extends DatabaseSchema
{

    public function execute()
    {
        $this->databaseSchemaInstance->create("schools", function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }
}
