<?php

namespace App\database\migrations;

class BoardSchoolSchema extends DatabaseSchema
{

    public function execute()
    {
        $this->databaseSchemaInstance->create("board_school", function ($table) {
            $table->integer('board_id')->unsigned();
            $table->foreign('board_id')->references('id')->on('boards');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }
}
