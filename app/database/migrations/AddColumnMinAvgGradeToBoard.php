<?php

namespace App\database\migrations;

class AddColumnMinAvgGradeToBoard extends DatabaseSchema
{

    public function execute()
    {
        $this->databaseSchemaInstance->table('boards', function ($table) {
            $table->enum('min_avg_grade', [6,7,8,9])->after('type');
        });
    }
}
