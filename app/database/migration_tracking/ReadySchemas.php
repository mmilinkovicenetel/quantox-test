<?php

use App\database\migrations\AddColumnMinAvgGradeToBoard;
use App\database\migrations\BoardSchema;
use App\database\migrations\BoardSchoolSchema;
use App\database\migrations\GradesSchema;
use App\database\migrations\SchoolSchema;
use App\database\migrations\StudentsSchema;

//Declare Database Schema before run migrations.
return [
    BoardSchema::class,
    StudentsSchema::class,
    SchoolSchema::class,
    BoardSchoolSchema::class,
    GradesSchema::class,
    AddColumnMinAvgGradeToBoard::class,
];
