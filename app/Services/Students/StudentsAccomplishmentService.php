<?php

namespace App\Services\Students;

use App\interfaces\service\ServiceDefaultInterface;
use App\models\Student;

class StudentsAccomplishmentService implements ServiceDefaultInterface
{
    private $response;
    private $student;

    public function __construct()
    {
    }

    public function execute()
    {
        $averageGrade = $this->student->grades->average('grade_value');

        $this->student->hasPassed();

        $this->response = [
            "student_id" => $this->student->id,
            "student_name" => $this->student->name,
            "list_of_grades" => $this->student->grades->pluck('grade_value')->toArray(),
            "average_grade" => $averageGrade,
            "final_result" => $this->student->hasPassed()
        ];

        return $this->response;
    }

    /**
     * Set student.
     *
     * @param Student $student
     * @return $this
     */
    public function setStudent(Student $student): self
    {
        $this->student = $student;
        return $this;
    }
}
