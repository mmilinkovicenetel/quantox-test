# README #

* Guide how to start application.

### What is this repository for? ###

* This is mini-app for quantox test application.

### How do I get set up? ###

* First step after cloning repo is to run command: composer install (composer update maybe)

* Configuration -> Every http request goes through index.php file in repo root. So you need to config path on server.

* Database configuration -> In bootrstrap.php file you neeed to change parameters for database connection. After that, create simple migration table with (id, schema_name) colums.

* Database configuration -> After that, you have two routes ("/run-migrations", "/run-seeders") call it to generate database tables and relationships and fill it with data. 
* (You will need to change those values if you want to test app)

* Database configuration -> I know, that this is worst practice for migratons and seeders, but this was the fast solution. I divided seeders and migrations in separete classess, so refactoring would be easy.

### Who do I talk to? ###

* If you have any questions, you can send me email: mmilinkovic1992@gmail.com